﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace express_management_system_gui
{
    /// <summary>
    /// AdminInfo.xaml 的交互逻辑
    /// </summary>
    public partial class AdminInfo : Window
    {
        public AdminInfo()
        {
            InitializeComponent();
        }

        private void BuLogout_Click(object sender, RoutedEventArgs e)
        {
            new LoginWindow();
            this.Close();
        }

        private void BuOpenOrderSet_Click(object sender, RoutedEventArgs e)
        {
            new BackgroundInfoQuerier().Show();
            this.Close();
        }

        private void BuOpenUserSet_Click(object sender, RoutedEventArgs e)
        {
            new BackgroundInfoQuerier().Show();
            this.Close();
        }
    }
}
