﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace express_management_system_gui
{
    /// <summary>
    /// UserInfoQuerier.xaml 的交互逻辑
    /// </summary>
    public partial class UserInfoQuerier : Window
    {
        public UserInfoQuerier()
        {
            InitializeComponent();
        }

        private void BuSendPackage_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ReceiveRecordPrevPage_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ReceiveRecordNextPage_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SendRecordPrevPage_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SendRecordNextPage_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void TbBack_MouseDown(object sender, MouseButtonEventArgs e)
        {
            new UserInfoWindow().Show();
            this.Close();
        }

        private void SendDelete0_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SendDelete1_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SendDelete2_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ReceiveDelete0_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ReceiveDelete1_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ReceiveDelete2_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
