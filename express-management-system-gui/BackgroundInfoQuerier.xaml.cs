﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace express_management_system_gui
{
    /// <summary>
    /// BackgroundInfoQuerier.xaml 的交互逻辑
    /// </summary>
    public partial class BackgroundInfoQuerier : Window
    {
        public BackgroundInfoQuerier()
        {
            InitializeComponent();
        }

        private void TbBack_MouseDown(object sender, MouseButtonEventArgs e)
        {
            new AdminInfo().Show();
            this.Close(); 
        }

        private void PackageEdit0_Click(object sender, RoutedEventArgs e)
        {

        }

        private void PackageEdit1_Click(object sender, RoutedEventArgs e)
        {

        }

        private void PackageEdit2_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SendRecordPrevPage_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SendRecordNextPage_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BuEdit0_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BuEdit1_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BuEdit2_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ReceiveRecordPrevPage_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ReceiveRecordNextPage_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SearchReceivedPackage_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SearchSendedPackage_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
