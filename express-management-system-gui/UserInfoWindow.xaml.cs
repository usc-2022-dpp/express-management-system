﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace express_management_system_gui
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class UserInfoWindow : Window
    {
        public UserInfoWindow()
        {
            InitializeComponent();
        }

        bool change = false;
        private void BuChangeUserInfo_Click(object sender, RoutedEventArgs e)
        {
            change = !change;
            TbAddress.IsEnabled = change;
            TbPhoneNumber.IsEnabled = change;
            TbEMailAddress.IsEnabled = change;
///         
        }

        private void BuLogout_Click(object sender, RoutedEventArgs e)
        {
            var nextWindow = new LoginWindow();
            nextWindow.Show();
            this.Close();
        }

        private void BuOpenHistory_Click(object sender, RoutedEventArgs e)
        {
            new UserInfoQuerier().Show();
            this.Close();
        }
    }
}
