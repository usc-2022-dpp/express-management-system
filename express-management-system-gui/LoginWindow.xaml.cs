﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace express_management_system_gui
{
    /// <summary>
    /// LoginWindow.xaml 的交互逻辑
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void BuLogin_Click(object sender, RoutedEventArgs e)
        {
            //xaml文件第26行：                <TextBox Width="150" Name="TbAccount"/>
            var account = TbAccount.Text;
            //xaml文件第31行：                <TextBox Width="150" Name="TbPassword"/>
            var password = TbPassword.Text;
            //TODO:连接数据库并查找符合条件的用户，找到后按照用户类型跳转到对应的界面
            //new UserInfoWindow().Show();
            //new AdminInfo().Show();
            this.Close();
        }
    }
}
