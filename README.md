# 快递管理系统

#### 介绍
使用MySQL+WPF+python构建的快递管理系统

#### 如何将仓库拉取到本地
1.  打开cmd，输入cd 项目保存路径
2.  左侧显示的路径会改变，说明成功切换了路径
3.  输入git clone https://gitee.com/usc-2022-dpp/express-management-system.git

#### 如何更新仓库
1.  打开cmd，输入cd 项目保存路径
2.  左侧显示的路径会改变，说明成功切换了路径
3.  输入git pull

#### 项目结构介绍
1.  在src目录下有三个文件夹，分别是views、viewmodules和modules。其中一个窗口在views和viewmodules下的代码文件名要一致
> 例如，对于窗口MainWindow，则在views下应当有一个MainWindow.ui文件，在viewmodules下有一个MainWindow.py文件
2.  views文件夹下存放desginer生成的.ui文件
3.  viewmodules文件夹下存放处理对应窗口ui逻辑的py文件，并负责在加载窗口时加载views下的对应.ui文件
4.  modules文件夹下存放公共的逻辑代码，例如处理数据库连接的代码
5.  项目使用WPF编译生成的程序进行启动，通过命令行执行py文件与pyhon代码交互，当然建议直接C#一把梭

#### 如何创建git提交
这里只提供命令行形式的，使用IDE内置git功能的请百度
1.  在项目根目录下启动命令行
2.  输入 git add .将所有修改过的文件提交的暂存区
3.  输入 git -m “提交前缀:你的提交说明” 提交暂存区的文件
4.  输入 git -push 推送到远程
\建议使用IDE内部提供的git功能，上述过程只是一次提交的示例。

#### 提交规范
使用下列前缀简短概括提交类型，常用的有feat、fix和refactor<br>

> 例如：feat:提供了界面登录功能<br>
> 注意：这里的“:”是英文符号<br>
> 也可以查看git提交记录来观察提交规范

feat: 新功能、新特性<br>
fix: 修改 bug<br>
perf: 更改代码，以提高性能（在不影响代码内部行为的前提下，对程序性能进行优化）<br>
refactor: 代码重构（重构，在不影响代码内部行为、功能下的代码修改）<br>
docs: 文档修改<br>
style: 代码格式修改<br>
test: 测试用例新增、修改<br>
build: 影响项目构建或依赖项修改<br>
revert: 恢复上一次提交<br>
ci: 持续集成相关文件修改<br>
chore: 其他修改（不在上述类型中的修改）<br>
release: 发布新版本<br>
workflow: 工作流相关文件修改<br>

