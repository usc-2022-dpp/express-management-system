import sys
import PySide2.QtWidgets
from PySide2.QtUiTools import QUiLoader


def main():
    qt_app =  PySide2.QtWidgets.QApplication(sys.argv)
    QUiLoader.load("/views/LoginWindow.ui")
    sys.exit(qt_app.exec_())

if __name__ == '__main__':
    main()